class NumberGuess
  def initialize
    @mystery_number = Random.rand(10)
  end

  def correct?(guess)
    guess == @mystery_number
  end

  def hint(guess)
    if guess > @mystery_number
      puts "\nLower..."
    else
      puts "\nHigher..."
    end
  end

  def ask_for_number
    puts("\nWhat number am I thinking of?")
    gets.chomp.to_i
  end
end

number_guess = NumberGuess.new

loop do
  guess = number_guess.ask_for_number

  break if number_guess.correct?(guess)

  number_guess.hint(guess)
end

puts "You got it!"
